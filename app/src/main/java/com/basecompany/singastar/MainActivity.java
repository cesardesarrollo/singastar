package com.basecompany.singastar;

import android.app.Activity;
import android.content.Intent;
import android.os.Bundle;

import com.basecompany.singastar.utils.AppUtils;
import com.basecompany.singastar.views.activities.SplashActivity;


public class MainActivity extends Activity {
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        AppUtils.printKeyHash(this);

        Intent intent = SplashActivity.newIntent(this);
        startActivity(intent);
    }
}
