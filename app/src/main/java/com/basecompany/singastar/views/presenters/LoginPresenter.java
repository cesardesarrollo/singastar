package com.basecompany.singastar.views.presenters;

import com.basecompany.singastar.views.BaseView;
import com.basecompany.singastar.views.presenters.base.BasePresenter;

public class LoginPresenter implements BasePresenter {
    private LoginView view;

    public LoginPresenter(LoginView view) {
        this.view = view;
    }

    public void login(String user, String pass) {
        if (user == null || user.isEmpty()) {
            view.userError("Empty User");
            return;
        }
        if (pass == null || pass.isEmpty()) {
            view.passwordError("Empty Password");
            return;
        }

        view.loginSucceeded();
    }

    @Override
    public void resume() {

    }

    @Override
    public void pause() {

    }

    @Override
    public void stop() {

    }

    @Override
    public void destroy() {

    }

    @Override
    public void onError(String message) {

    }

    public interface LoginView extends BaseView {
        void userError(String message);

        void passwordError(String message);

        void loginSucceeded();
    }
}