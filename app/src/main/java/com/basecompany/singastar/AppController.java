package com.basecompany.singastar;

import android.app.Application;
import android.content.Context;

import com.facebook.FacebookSdk;
import com.facebook.appevents.AppEventsLogger;

public class AppController extends Application {
    private static AppController instance;

    public AppController() {
        instance = this;
    }

    @Override
    public void onCreate() {
        super.onCreate();
        FacebookSdk.sdkInitialize(getApplicationContext());
        AppEventsLogger.activateApp(this);
    }

    /**
     * Never use this method for UI Context
     */
    public static Context getContext() {
        return instance;
    }
}
